import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import createHistory from 'history/createHashHistory';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'react-router-redux';
import { loadingBarMiddleware } from 'react-redux-loading-bar'

import rootReducer from '../reducers';

const history = createHistory();
const middlewareRouter = routerMiddleware(history);

const enhancer = composeWithDevTools(
  applyMiddleware(middlewareRouter, thunk, loadingBarMiddleware({promiseTypeSuffixes: ['INIT', 'SUCCESS', 'FAILURE'],}), createLogger())
);

export default function configureStore (initialState) {
  return createStore (rootReducer, initialState, enhancer);
}
