import {
  FETCH_COMPANIES_INIT,
  FETCH_COMPANIES_FAILURE,
  FETCH_COMPANIES_SUCCESS,
  FETCH_COMPANY_INIT,
  FETCH_COMPANY_FAILURE,
  FETCH_COMPANY_SUCCESS,
  SEARCH_INIT,
  SEARCH_FAILURE,
  SEARCH_SUCCESS 
} from './types';

import CompanyAPI from '../api/company';

export function fetchCompaniesInit () {
  return { type: FETCH_COMPANIES_INIT };
};

export function fetchCompaniesFailure (error) {
  return {
    type: FETCH_COMPANIES_FAILURE,
    payload: error,
  };
};

export function fetchCompaniesSuccess (companies) {
  return {
    type: FETCH_COMPANIES_SUCCESS,
    payload: companies,
  };
};

export function fetchCompanies () {
  return async (dispatch) => {
    dispatch(fetchCompaniesInit());
    
    try {
      let response = await CompanyAPI.fetchCompanies();
      return dispatch(fetchCompaniesSuccess(response));
    } catch (error) {
      return dispatch(fetchCompaniesFailure(error));
    }
  };
};

export function fetchCompanyInit () {
  return { type: FETCH_COMPANY_INIT };
};

export function fetchCompanyFailure (error) {
  return {
    type: FETCH_COMPANY_FAILURE,
    payload: error,
  };
};

export function fetchCompanySuccess (companyId) {
  return {
    type: FETCH_COMPANY_SUCCESS,
    payload: companyId,
  };
};

export function fetchCompany(companyId) {
  return async (dispatch) => {
    dispatch(fetchCompanyInit());

    try {
      let response = await CompanyAPI.fetchCompany(companyId);
      return dispatch(fetchCompanySuccess(response));
    } catch (error) {
      return dispatch(fetchCompanyFailure(error));
    }
  };
};

export function searchInit () {
  return { type: SEARCH_INIT };
};

export function searchFailure (error) {
  return {
    type: SEARCH_FAILURE,
    payload: error,
  };
};

export function searchSuccess (keyword) {
  return {
    type: SEARCH_SUCCESS,
    payload: keyword,
  };
};

export function search(keyword) {
  return async (dispatch) => {
    dispatch(searchInit());

    try {
      return dispatch(searchSuccess(keyword));
    } catch (error) {
      return dispatch(searchFailure(error));
    }
  };
};


