import React, { Component} from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'; // Para bindear/enlazar las acciones como propiedades (lo podemos llamar con this.props)
import { bindActionCreators } from 'redux'; // Nos va a permitir conectar el estado de la store como propiedades dentro de este componente
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Company from '../Company';

import * as companyActions from '../../actions/companyActions';

import './fullcompanydata.css';

class FullCompanyData extends Component {
    
    static propTypes = {
        companies: PropTypes.object.isRequired,
    }

    /** Al montarse el componente tomamos el id que nos llega por url y hacemos la busqueda de esa company**/
    async componentDidMount() {
      await this.props.companyActions.fetchCompany(this.props.match.params.id);
    }

    renderCompany() {
        let companies = this.props.companies.data;
        
        if (companies.length > 0) {
            return companies.map((c, i) => <Company company={c} fullData={true} onCompanyClick={this.handleOnCompanyClick} key={i}/>);
        } else {
            return <h4>No data! :/</h4>
        }
    }

    render() {
        return (  
            <div className="container full-data-container animated fadeIn">
                <div className="row d-flex align-items-center data-company-wrapper">  
                    <div className="col-md-1">
                        <Link to={'/'}>
                            <span className="fa fa-chevron-left fa-3x"></span>
                        </Link>    
                    </div>
                    <div className="col-md-11 d-flex align-items-center justify-content-center">
                        { 
                            this.renderCompany()
                        }         
                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps (state) {
    return {
        companies: state.companies,
    };
}

function mapDispatchToprops (dispatch) {
    return {
        companyActions: bindActionCreators(companyActions, dispatch),
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToprops)(FullCompanyData));
