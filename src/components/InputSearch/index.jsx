import React from 'react';
import './input-search.css';

const InputSearch = ({ onSearch }) => 

<div className="input-group mb-3">
  <input 
    type="text" 
    className="form-control input-search" 
    placeholder="Search..." 
    aria-label="Username" 
    aria-describedby="basic-addon1" 
    onChange={(e) => onSearch(e.target.value)}
    />
</div>

export default InputSearch;