import React from 'react';

import Routes from '../Routes';

import './app-container.css';

const App = () =>
<div>
  <Routes/>
</div>

export default (App);
