import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'; // Para bindear/enlazar las acciones como propiedades (lo podemos llamar con this.props)
import { bindActionCreators } from 'redux'; // Nos va a permitir conectar el estado de la store como propiedades dentro de este componente
import { connect } from 'react-redux';
import LoadingBar from 'react-redux-loading-bar';
import Company from '../Company';
import InputSearch from '../InputSearch';

import './companies.css';

import * as companyActions from '../../actions/companyActions';


class Companies extends Component {
  
  constructor(props) {
    super(props);
    this.handleOnSearch = this.handleOnSearch.bind(this);
  }

  static propTypes = {
    companies: PropTypes.object.isRequired,
  }

  /** Al montarse el compomente realizamos la peticion para obtener todas las companies **/
  async componentDidMount() {
    await this.props.companyActions.fetchCompanies();
  }

  /** Al buscar en el input lanzamos la accion encargada de la busqueda**/
  handleOnSearch(keyword)  {
    this.props.companyActions.search(keyword);
  }

  renderCompanies() {
    let companies = this.props.companies.data;
    let searchs = this.props.companies.search;
    
    /** 
     * Si existen companies en el estado de la app, se muestran las mismas
     * Caso contrario se muestran las companies cargadas o html básico.
     */
    if (searchs.length > 0) {
      return searchs.map((c, i) => <Company company={c} fullData={false} key={i}/>);
    }else if (companies.length > 0) {
      return companies.map((c, i) => <Company company={c} fullData={false} key={i}/>);
    } else {
      return <h4 className="d-flex align-items-center justify-content-center">No data! :/</h4>;
    }
  }

  render() {
    return (
      <div className="container main-container animated fadeIn">
        <div className="row fixed-top">
          <LoadingBar /> 
        </div>
        <div className="row input-search-wrapper">
            <div className="col-md-8 offset-md-2 col-xs-12 ">
              <InputSearch onSearch={this.handleOnSearch}/>
            </div>
        </div>
        <div className="row">
          <div className="col-md-8 offset-md-2 col-xs-12 companies-wrapper" >
            { 
              this.renderCompanies()
            }
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    companies: state.companies,
  };
}

function mapDispatchToprops (dispatch) {
  return {
    companyActions: bindActionCreators(companyActions, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToprops)(Companies));

