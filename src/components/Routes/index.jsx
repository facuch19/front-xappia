import React from 'react';
import { Switch, Route  } from 'react-router-dom';
import './routes.css';
import Main from '../Main';
import FullCompanyData from '../FullCompanyData';
import NotFound from '../NotFound';

const Routes = () => (
  <main>
    <Switch>
      <Route  path="/" component={Main} />
      <Route  path="/:id" component={FullCompanyData} />
      <Route path="*" component={NotFound} />
    </Switch>
  </main>
);

export default Routes;
