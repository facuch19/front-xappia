import React from 'react';
import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './company.css';

const Company = ({ company, fullData }) => {
    /*
     * Si 'fullData' llega como falso, solo mostramos 3 propuedades del objeto company
     * Caso contrario recorremos y mostramos todos sus keys-values      
     */
    if (!fullData) {
        return (
            <Link className="nav-link" to={`/${company.id}`}>
                <Alert bsStyle="warning">
                        <div className="row animated fadeIn"  >
                            <div className="col-md-4 col-xs-12 single-data-row">
                                <strong className="company-property">First Name:</strong> {company.firstName}
                            </div>
                            <div className="col-md-4 col-xs-12 single-data-row">
                                <strong className="company-property"> Last Name: </strong>{company.lastName}  
                            </div>
                            <div className="col-md-4 col-xs-12 single-data-row">
                                <strong className="company-property">  Age: </strong> {company.age}
                            </div>
                        </div>
                </Alert>
            </Link>    
        );   
    } else {
        return (
            <Alert bsStyle="warning">
                <div className="row animated fadeIn"  >
                    {   /** Reorremos la company y mostramos sus keys-values **/
                        Object.keys(company).map(key => 
                            <div className="col-md-4 col-xs-12 full-data-row">
                                <strong className="company-property">{ key.toUpperCase() + ':'}</strong> { company[key] }
                            </div>
                        )
                    }
                </div>      
            </Alert>
        );
    }
};

export default Company;