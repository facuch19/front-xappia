
import React from 'react';
import { Link } from 'react-router-dom';
import './not-found.css';

const NotFound = () =>
  <div className="container-fluid root-notFound">
    <div className="row not-found-container animated fadeIn">
      <Link to="/" className="come-back-button" ><i className="fa fa-arrow-circle-o-left fa-4x " aria-hidden="true"></i></Link>
      <h1 className="not-found-title">
        404 <small>Page Not Found :(</small>
      </h1>
    </div>
  </div>;

export default NotFound;
