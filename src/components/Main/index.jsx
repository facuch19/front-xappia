import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Companies from '../Companies';
import FullCompanyData from '../FullCompanyData';
import NotFound from '../NotFound';

import './main.css';

const Main = () =>
<Switch>
  <Route exact path="/" component={Companies}/>
  <Route path="/:id" component={FullCompanyData}/>
  <Route path="*" component={NotFound}/>
</Switch>;

export default Main;