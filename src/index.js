import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

import configureStore from './store/configureStore';
import initialState from './reducers/initialState';

import './index.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
//import 'bootstrap/dist/js/bootstrap.min.js';
import 'animate.css/animate.min.css';

const store = configureStore(initialState);

ReactDOM.render((
  <Provider store={store} >
    <Router >
      <App />
    </Router>
  </Provider>
), document.getElementById('root'));
registerServiceWorker();
