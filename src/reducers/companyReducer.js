import {
  FETCH_COMPANIES_INIT,
  FETCH_COMPANIES_FAILURE,
  FETCH_COMPANIES_SUCCESS,
  FETCH_COMPANY_INIT,
  FETCH_COMPANY_FAILURE,
  FETCH_COMPANY_SUCCESS,
  SEARCH_INIT,
  SEARCH_FAILURE,
  SEARCH_SUCCESS 
} from '../actions/types';

import initialState from './initialState';

export default function companyReducer (state = initialState.companies, action) {
  switch (action.type) {

    case FETCH_COMPANIES_INIT:
      return {
        ...state,
        loading: true,
        error: null,
        data: [],
      };

    case FETCH_COMPANIES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        data: [],
      };

    case FETCH_COMPANIES_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        data: action.payload,
      };

    case FETCH_COMPANY_INIT:
      return {
        ...state,
        loading: true,
        error: null,
        data: [],
      };

    case FETCH_COMPANY_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        data: [],
      };

    case FETCH_COMPANY_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        data: action.payload,
      };

    case SEARCH_INIT:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case SEARCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case SEARCH_SUCCESS:

      let keyword = action.payload;
      let companies = state.data;
      let search = [];
      
      /*
       * Lógica para buscar coincidencias en las companias cargadas en el state de la app,
       * con la busqueda realizada por el usuario.
       * 
       * Se recorre el arreglo de objetos company. 
       * Y se recorren los valores de las propiedades de los mismos.
       * Si alguna de los valores de estos objetos 'coincide' (comparacion de strings)
       * con la busqueda realizada por el user, se devuelve este objeto.
       *
       */

      companies.forEach(c => {
        let values = Object.values(c);
        
        for (let i = 0; i < values.length; i++) {
          let element = values[i];

          //Necesitamos convertir cualquier propiedad que sea del tipo number a string, para poder utilizar el metodo indexOf y comparar su valor con la keyword que nos llega de la busqueda.
          if (typeof(element) === 'number') {
            element = `'${element}'`; 
          } 

          if(element.indexOf(keyword) >= 0) {
            search.push(c);
            break;
          }   
        }
      });  
      
      return {
        ...state,
        loading: false,
        error: null,
        search: search
      };
      
    default:
      return state;
  }
};
