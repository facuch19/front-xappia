import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { loadingBarReducer } from 'react-redux-loading-bar'
import companies from './companyReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  loadingBar: loadingBarReducer,
  companies,
});

export default rootReducer;
