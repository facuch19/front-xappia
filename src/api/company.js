import 'whatwg-fetch';

/*
 * Variable de entorno para que nuestra API funcione en conjunto con REACT.
 * 
 * La misma hace referencia a un proxy.
 * 
 * Por motivos de CORS, en el package.json esta definido el PROXY y la verdadera
 * URL de nuestra API. 
 */
const API_URL = '/api/';


const CompaniesAPI = {
  
  async fetchCompanies () {
    const response = await fetch(API_URL, {
      method: 'GET',
    });
    
    const data = await response.json();

    if (data.code !== 200)
      throw new Error(`code: ${data.code}, message: ${data.message}`);

    return data.data;
  },
  
  async fetchCompany (id) {
    const response = await fetch(API_URL+id, {
      method: 'GET',
    });

    const data = await response.json();

    if (data.code !== 200)
      throw new Error(`code: ${data.code}, message: ${data.message}`);

    return data.data;
  },
};

export default CompaniesAPI;
