# Detalles

Se utilizó entre algunas de las dependencias principales
  - react v16
  - bootstrap v.4
  - react-loading-bar (En mi caso react-redux-loading-bar).
  - react-router v 4.1 (react-router-dom)
  - redux v3.7 
  - whatwg-fetch v2.0
  - prop-types

# Implementar un buscador en la lista de recursos.
Se implementó un pequeño buscador, con logica desarrollada por mi que busca directamente en el store de la aplicacion sin necesidad de hacer llamadas extras a la API. .